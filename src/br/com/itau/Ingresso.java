package br.com.itau;

public class Ingresso {
    public double valor;

    public Ingresso(double valor) {
        this.valor = valor;
    }

    public double getImprimeValor(){
        return valor;
    }
}

package br.com.itau;

public class Normal extends Ingresso {
    public int qntDeIngressos;

    public Normal(double valor, int qntDeIngressos) {
        super(valor);
        this.qntDeIngressos = qntDeIngressos;
    }

    @Override
    public double getImprimeValor() {
        return super.getImprimeValor() * qntDeIngressos;
    }
}

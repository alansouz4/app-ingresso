package br.com.itau;

public class Vip extends  Ingresso{
    public double valorAcidional;

    public Vip(double valor, double valorAcidional) {
        super(valor);
        this.valorAcidional = valorAcidional;
    }

    @Override
    public double getImprimeValor() {
        return super.getImprimeValor() + valorAcidional;
    }
}

package br.com.itau;

public class Main {

    public static void main(String[] args) {

        Normal normal = new Normal(10.0, 2);
        System.out.println(normal.getImprimeValor());

        Vip vip = new Vip(10, 15);
        System.out.println(vip.getImprimeValor());

    }
}
